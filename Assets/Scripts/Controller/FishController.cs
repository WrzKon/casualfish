﻿using UnityEngine;
using System.Collections;

public class FishController : MonoBehaviour {

    BackgroundManager backgroundManager;
    Rigidbody rb;

    public int number;
    

    void Awake()
    {
        backgroundManager = FindObjectOfType<BackgroundManager>();
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        SetDirection();
    }

    void Update()
    {
        
        if (!backgroundManager.IsInRange(transform.position))
        {           
            SetDirection();
        }
    }
          
    void SetDirection()
    {
        Vector3 startPosition = backgroundManager.DrawStartPosition(number);
        Vector3 endPosition = backgroundManager.DrawEndPosition(startPosition);
        Quaternion rotation = Quaternion.LookRotation(endPosition - startPosition);

        transform.position = startPosition;
        transform.rotation = rotation;

        int speed = Random.Range(50, 80);

       ForceReset();

        rb.AddForce((endPosition - startPosition).normalized * speed);
        transform.localScale = backgroundManager.DrawFishScale();
    }      

    void ForceReset()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

}
