﻿using UnityEngine;
using System.Collections;

public class TelescopeFollow : MonoBehaviour {

    public PlayerController player;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, player.transform.position, 10.0f);
    }
}
