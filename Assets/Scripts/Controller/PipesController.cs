﻿using UnityEngine;
using System.Collections;

public class PipesController : MonoBehaviour {

    GameManager gameManager;
    float speed;
    GameObject pipeParent;
    


    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        speed = gameManager.pipesSpeed;

        pipeParent = GameObject.Find("Pipes");
        transform.parent = pipeParent.transform;
    }

    void Update () {

        if(gameManager.pipeLoop)
            PipeMoving();

        if (transform.position.x < -5.0f)        
            gameManager.PipePositionSet(transform);
            
    }

    void PipeMoving()
    {
        transform.position += new Vector3(-speed * Time.deltaTime, 0f, 0f);
    }




}
