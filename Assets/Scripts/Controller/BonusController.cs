﻿using UnityEngine;
using System.Collections;

public class BonusController : MonoBehaviour {

    GameManager gameManager;
    float speed;
    PlayerController player;


    void Start()
    {
        player = FindObjectOfType<PlayerController>();

        gameManager = FindObjectOfType<GameManager>();
        speed = gameManager.pipesSpeed;
    }

        void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);

        if(!player.isDead)
        {
            transform.position += new Vector3(-speed * Time.deltaTime, 0f, 0f);
        }
        

        if (transform.position.x < -5.0f)
            Destroy(gameObject);
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag != "Player")
        {
            transform.position += new Vector3(Random.Range(2.0f, 4.0f), 0, 0);
        }
    }
}
