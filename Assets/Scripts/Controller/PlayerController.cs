﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;

    [HideInInspector]public Vector3 jumpVelocity;
    public float jumpPower = 9f;    
    public float rotationIntesity = 4f;
    [HideInInspector]public int score;
    [HideInInspector]public bool isDead;
    public UIManager uiManager;
    public GameObject bubbles;
    public float bubblesSpeed;
    public AudioSource colliderAudio;
    public AudioSource jumpAudio;
    public AudioClip pointClip;
    public AudioClip dieClip;
    public AudioClip bonusClip;
    [HideInInspector]public CapsuleCollider fishCollider;
    public ModesManager modesManager;
    [HideInInspector]public bool godMode = false;
    public int scoreMultipler = 1;
    public Animator animManager;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jumpVelocity = new Vector3(0, jumpPower, 0);
        isDead = true;

        fishCollider = GetComponent<CapsuleCollider>();

        score = 0;
    }

    void Update()
    {
        if (!IsInCameraRange(transform) && !godMode)
        {
            isDead = true;
        }

        bool click;
#if UNITY_EDITOR
        click = Input.GetKeyDown(KeyCode.Space);
#elif UNITY_ANDROID
        click = Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
#endif
        if (click && !isDead) 
        {
            rb.velocity = jumpVelocity;

            GameObject bubbleInstance = Instantiate(bubbles, transform.position + new Vector3(-.7f,.8f,0f), Quaternion.identity) as GameObject;
            bubbleInstance.transform.GetComponent<Rigidbody>().velocity = new Vector3(-bubblesSpeed, 0, 0);
            Destroy(bubbleInstance.gameObject, 5);

            jumpAudio.Play();
        }
    }

    void FixedUpdate()
    {
            Quaternion from = rb.rotation;
            Quaternion to = Quaternion.Euler(rb.velocity.y * rotationIntesity * (-1), 90.0f, 0.0f);

            rb.rotation = Quaternion.Lerp(from, to, Time.fixedDeltaTime * 10f);        
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "Pipe")
        {
            if (!godMode && !isDead)
            {
                isDead = true;
                fishCollider.enabled = false;
            }           
        }

        else if(coll.tag == "PointArea")
        {
            if (!isDead)
            {
                score += scoreMultipler;
                uiManager.ScoreTextSet(score);

                colliderAudio.clip = pointClip;
                colliderAudio.Play();
            }
        }

        else if(coll.tag == "Bonus")
        {
            colliderAudio.clip = bonusClip;
            colliderAudio.Play();

            Destroy(coll.gameObject);
            modesManager.CoroutineStarter();
        }
    }  
    
    public void Death()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, -5);
        rb.velocity = new Vector3(1, 10, 0);

        colliderAudio.clip = dieClip;
        colliderAudio.Play();
    }

    bool IsInCameraRange(Transform objectTransform)
    {
        float y = objectTransform.position.y;

        return y < 5 && y > -5;
    }    
}
