﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginController : MonoBehaviour {

    string defaultNick = "Player";
    [SerializeField]
    InputField input;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("Nick"))
        {
            input.text = PlayerPrefs.GetString("Nick");
        }
        else
            input.text = defaultNick;

        input.onValidateInput += delegate (string input, int charIndex, char addedChar) { return MyValidate(addedChar); };
    }

    public void Click()
    {
        PlayerPrefs.DeleteAll();
        if(string.IsNullOrEmpty(input.text))
            PlayerPrefs.SetString("Nick", defaultNick);
        else
            PlayerPrefs.SetString("Nick", input.text);

        PlayerPrefs.Save();
        string name = PlayerPrefs.GetString("Nick");

        SceneManager.LoadScene("Scene1");
    }

    private char MyValidate(char charToValidate)
    {
        Regex rgx = new Regex(@"[a-zA-Z0-9]");

        if (!rgx.IsMatch(charToValidate.ToString()))
        {
            charToValidate = '\0';
        }
        return charToValidate;
    }
}
