﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour
{
    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void TriggerSet(string triggerName)
    {
        anim.SetTrigger(triggerName);
    }  
       
}
