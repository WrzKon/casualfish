﻿using UnityEngine;
using System.Collections;

public class BackgroundMusicManager : MonoBehaviour {

    public AudioClip[] clips;
    public AudioSource backgroundMusic;
    public AudioSource modeMusic;

    int lastClip;

    void Start()
    {
        lastClip = Random.Range(0, clips.Length);

        backgroundMusic = GetComponent<AudioSource>();
        backgroundMusic.clip = clips[lastClip];
        backgroundMusic.Play();
    }

    void Update()
    {        
        if (!backgroundMusic.isPlaying)
        {
            int newClip = DrawClip(lastClip);
            lastClip = newClip;

            backgroundMusic.clip = clips[newClip];
            backgroundMusic.Play();
        }
    }


    int DrawClip(int lastClipIndex)
    {
        int newClipIndex = Random.Range(0, clips.Length);

        if(newClipIndex == lastClipIndex)
        {
           return DrawClip(lastClipIndex);
        }

        else
        {
            return newClipIndex;
        }
    }
}
