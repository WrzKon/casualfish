﻿using UnityEngine;
using System.Collections;


public class BonusSpawnManager : MonoBehaviour {

    public PlayerController player;
    public GameObject box;
    public GameManager gameManager;

    bool spawnFlag = false;
    float timer;
    

    
    void Start()
    {
        timer = Random.Range(3.0f, 8.0f);
    }

    void Update()
    {
        if (gameManager.pipeLoop)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
                spawnFlag = true;

            if (spawnFlag)
            {
                Vector3 position = new Vector3(18f, Random.Range(-2.5f, 2.5f), 0);
                Quaternion rotation = Quaternion.Euler(20, -145, 0);
                Instantiate(box, position, rotation);

                spawnFlag = false;
                timer = Random.Range(9.0f, 20.0f);
            }
        }
        
        
    }
        
    
}

