﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public int distanceBetweenPipes = 6;
    public float pipesSpeed = 3;
    public float minPipeYPosition = -2.5f;
    public float maxPipeYPosition = 2.5f;    
    public GameObject pipe;
    public bool pipeLoop = false;
    public float startDelay = 2.0f;
    public PlayerController player;
    public Camera mainCamera;
    public UIManager uiManager;
    public ModesManager modeManager;
    public AnimationManager animManager;
    [SerializeField]
    OnlineDatabase onlineDatabase;

    bool menu;    
    
    void Start()
    {
        StartCoroutine(GameLoop());       
    }
    
    void Update()
    {
        if(menu && Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }
    }
           

    IEnumerator GameLoop()
    {
        while (true)
        {
            yield return StartCoroutine(GameStart());
            yield return StartCoroutine(GameEnd());
            yield return StartCoroutine(GameRestart());
        }
    }

    void SpawnPipes()
    {
        PipeDestroy();
        GravityForceSet(0.0f);

        pipeLoop = false;

        for(int i=0; i<4*distanceBetweenPipes; i += distanceBetweenPipes)
        {
            Vector3 position = new Vector3(i + 15, PipeYPositionSet(), 0);
            Instantiate(pipe, position, Quaternion.identity);
        }
    }

    IEnumerator GameStart()
    {
        uiManager.SetScoreText(new List<Score>());
        SpawnPipes();
        CameraPositionSet(0,-.4f, 2.0f);
        uiManager.StartTextSet();
        

        while (true)
        {
            while (true)
            {
#if UNITY_EDITOR
                if (Input.GetKeyDown(KeyCode.Space))
                    break;
#elif UNITY_ANDROID
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) 
                    break;
#endif
                yield return null;
            }

            animManager.TriggerSet("GameRun");
            menu = false;

            CameraPositionSet(4.5f, 0, 5.0f);
            uiManager.TimeCounter(startDelay);
            yield return new WaitForSeconds(startDelay);
            
            pipeLoop = true;
            GravityForceSet(20.0f);
            player.isDead = false;
            yield return null;
            break;           
        }

    }
    

    IEnumerator GameEnd()
    {
        uiManager.ScoreTextSet(player.score);
        
        while (true)
        {
            while (!player.isDead) yield return null;

            if (player.score > 0) 
                onlineDatabase.InsertAndGetBestScores(PlayerPrefs.GetString("Nick"), player.score);
            else
                onlineDatabase.GetBestScores();

            player.Death();
            animManager.TriggerSet("IsDead");
            pipeLoop = false;
            yield return null;
            break;
        }

    }

    IEnumerator GameRestart()
    {
        yield return new WaitForSeconds(2f);
        uiManager.EndTextSet(player.score);
        uiManager.SetScoreText(onlineDatabase.scores);
        while (true)
        {
            while (true)
            {
#if UNITY_EDITOR
                if (Input.GetKeyDown(KeyCode.Space))
                    break;
#elif UNITY_ANDROID
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) 
                    break;
#endif
                yield return null;
            }
            
            yield return new WaitForSeconds(0.5f);
            animManager.TriggerSet("BackToMenu");
            menu = true;

            GravityForceSet(0);
            PlayerSpawn();
            BonusDestroy();
            modeManager.witchMode = 1000;
            yield return null;
            break;
        }

    }
    

    public void PipePositionSet(Transform transform)
    {
        transform.position = new Vector3(transform.position.x + 4 * 6.0f, PipeYPositionSet(), 0f);
    }

    float PipeYPositionSet()
    {
        return Random.Range(minPipeYPosition, maxPipeYPosition);
    }

    void PipeDestroy()
    {
        PipesController[] pipeArray = FindObjectsOfType<PipesController>();

        for (int i = 0; i < pipeArray.Length; i++)
        {
            Destroy(pipeArray[i].gameObject);
        }
    }

    void GravityForceSet(float gravityForce)
    {
        Physics.gravity = new Vector3(0, (-1) * gravityForce, 0);
    }

    void PlayerSpawn()
    {
        player.transform.position = Vector3.zero;
        player.transform.rotation = Quaternion.identity;
        player.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
        player.score = 0;
        player.fishCollider.enabled = true;       
    }

    void CameraPositionSet(float x, float y, float size)
    {
        mainCamera.transform.position = new Vector3(x, y, -10);
        mainCamera.orthographicSize = size;
    }

    void BonusDestroy()
    {
        BonusController[] bonus = FindObjectsOfType<BonusController>();

        for (int i = 0; i < bonus.Length; i++)
        {
            Destroy(bonus[i].gameObject);
        }
    }
}
