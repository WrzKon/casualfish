﻿using UnityEngine;
using System.Collections;

public class ModesManager : MonoBehaviour {

    public PlayerController player;
    public AudioClip godModClip;
    public AudioClip tsunamiClip;
    public AudioClip reflectionClip;
    public AudioClip telescopeClip;
    public AudioClip upAndDownClip;
    public BackgroundMusicManager musicManager;
    public float cameraShakePower = .05f;
    public Camera mainCamera;
    [HideInInspector]public int witchMode = 1000;
    public SpriteRenderer telescopeSprite;
    public UIManager uiManager;
    public AnimationManager animManager;

    bool isModeRun = false;
    bool tsunamiFlag = false;
    Vector3 cameraPosition;

    void Start()
    {
        cameraPosition = new Vector3(4.5f, 0, -10f);
    }

    void Update()
    {
        if(player.isDead && isModeRun)
        {
            StopCoroutine("DrawMode");
            StopMode();
        }

        if(tsunamiFlag)
        {
            Vector3 newCameraPosition = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0).normalized;
            mainCamera.transform.position = cameraPosition + newCameraPosition * cameraShakePower*Time.deltaTime;
        }

    }

    public void CoroutineStarter()
    {
        if (isModeRun)
        {
            StopCoroutine("DrawMode");
            StopMode();
        }
         
        StartCoroutine("DrawMode");
        isModeRun = true;
    }

    IEnumerator DrawMode()
    {
        int index = Random.Range(0, 6);
        witchMode = index;

        uiManager.ModeTextSet(index);
        animManager.TriggerSet("IsModeStart");

        switch (index)
        {
            case 1:
                Tsunami();
                break;

            case 2:
                Telescope();
                break;

            case 3:
                UpAndDown();
                break;

            default:
                GodMode();
                break;
        }

        yield return new WaitForSeconds(8f);
        StopMode();
        yield return null; 
    }

    void GodMode()
    {
        player.godMode = true;

        player.transform.localScale *= 3;
        player.transform.position += new Vector3(0, 0, -.55f);

        MusicOn(godModClip);        
    }

    void Tsunami()
    {
        tsunamiFlag = true;

        MusicOn(tsunamiClip);
    }

    void Reflection()
    {
        mainCamera.transform.rotation = Quaternion.Euler(0, 180, 0);

        MusicOn(reflectionClip);
    }

    void Telescope()
    {
        Color newTelescopeColor = telescopeSprite.color;
        newTelescopeColor.a = 1;
        telescopeSprite.color = newTelescopeColor;

        MusicOn(telescopeClip);
    }

    void UpAndDown()
    {
        Physics.gravity *= -0.8f;
        player.jumpVelocity *= -0.8f;

        MusicOn(upAndDownClip);
    }

    void MusicOn(AudioClip musicClip)
    {
        musicManager.modeMusic.clip = musicClip;
        musicManager.modeMusic.Play();
        musicManager.backgroundMusic.mute = true;

        player.scoreMultipler = 2;
    }


    void StopMode()
    {
        isModeRun = false;
        uiManager.ModeTextReset();

        switch (witchMode)
        {
            case 1:
                StopTsunami();
                break;

            case 2:
                StopTelescope();
                break;

            case 3:
                StopUpAndDown();
                break;

            default:
                StopGodMode();
                break;
        }
    }

    void StopGodMode()
    {
        player.godMode = false;
        player.transform.localScale /= 3;
        player.transform.position -= new Vector3(0, 0, -0.55f);

        MusicOff();
    }

    void StopTsunami()
    {
        tsunamiFlag = false;
        mainCamera.transform.position = cameraPosition;

        MusicOff();        
    }

    void StopReflection()
    {
        mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);

        MusicOff();
    }

    void StopTelescope()
    {
        Color newTelescopeColor = telescopeSprite.color;
        newTelescopeColor.a = 0;
        telescopeSprite.color = newTelescopeColor;

        MusicOff();
    }

    void StopUpAndDown()
    {
        Physics.gravity *= -1 / 0.8f;
        player.jumpVelocity *= -1 / 0.8f;

        MusicOff();
    }

    void MusicOff()
    {
        musicManager.modeMusic.Stop();
        musicManager.backgroundMusic.mute = false;

        player.scoreMultipler = 1;
    }

}
