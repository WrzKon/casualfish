﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIManager : MonoBehaviour {

    public Text upperText;
    public Text lowerText;
    public Text quitText;
    [SerializeField]
    Text scoreText;

    float timer = 0;
    bool countdown = false;
    string gameName = "Casual Fish";
    string modeString;

    void Awake()
    {
        modeString = gameName;
    }

    void Update()
    {
        if(countdown)
        {
            timer -= Time.deltaTime;
            lowerText.text = ((int)timer+1).ToString();
            if (timer<0)
            {
                countdown = false;
                lowerText.text = string.Empty;
            }
        }
    }

    public void StartTextSet()
    {
        upperText.text = gameName;
        lowerText.text = "Tap to start!";
        quitText.text = "";
    }

    public void ScoreTextSet(int score)
    {
        upperText.text = modeString;
        lowerText.text = "Score: " + score;  
    }

    public void EndTextSet(int score)
    {
        upperText.text = gameName;
        lowerText.text = "Your Score: "+score+ "\nTap to restart!";
    }

    public void TimeCounter(float delay)
    {
        timer = delay;
        countdown = true;

        quitText.text = string.Empty;
    }

    public void ModeTextSet(int modeIndex)
    {
        switch (modeIndex)
        {
            case 1:
                modeString = "Tsunami Mode!";
                break;

            case 2:
                modeString = "Cave Mode!";
                break;

            case 3:
                modeString = "Up And Down Mode!";
                break;

            default:
                modeString = "God Mode!";
                break;
        }
        upperText.text = modeString;
    }

    public void ModeTextReset()
    {
        modeString = gameName;
        upperText.text = modeString; 
    }

    public void SetScoreText(List<Score> scoreList)
    {
        if (scoreList.Count == 0)
            scoreText.text = "";
        else
        {
            string score = "SCORES:\n";
            for (int i = 0; i < scoreList.Count; ++i)
            {
                score += string.Format("{0}. {1}\t\t{2}\n", i + 1, scoreList[i].Nick, scoreList[i].Points);
            }
            scoreText.text = score;
        }
    }
}
