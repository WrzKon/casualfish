﻿using UnityEngine;
using System.Collections;

public class BackgroundManager : MonoBehaviour {
    
    public GameObject[] fishes;
    public float minScale = 0.3f;
    public float maxScale = 0.8f;
    public Transform fishParent;
    

    void Start()
    {
        StartCoroutine(FishSpawner());
    }

   
    IEnumerator FishSpawner()
    {
       for(int i=0; i<fishes.Length; i++)
        {
            GameObject instance = Instantiate(fishes[i], Vector3.zero, Quaternion.identity) as GameObject;
            instance.transform.parent = GameObject.Find("Fishes").transform;
            instance.GetComponent<FishController>().number = i;


            yield return new WaitForSeconds(Random.Range(1.0f, 5.0f));
        }
       
    }

   
    public bool IsInRange(Vector3 position)
    {
        return position.x > -8 && position.y < 7 && position.x < 17 && position.y > -7;
    }
    
    public Vector3 DrawEndPosition(Vector3 positionStart)
    {
        float x;
        float y = Random.Range(-5.0f, 5.0f);
        float z = positionStart.z;

        if (positionStart.x == -6)
            x = 15;
        else
            x = -6;

        return new Vector3(x, y, z);
    }

    public Vector3 DrawStartPosition(int fishIndex)
    {
        float x = DrawXPosition();
        float y = Random.Range(-5.0f, 5.0f);
        float z = fishIndex + 5;

        return new Vector3(x, y, z);
    }

    float DrawXPosition()
    {
        int site = Random.Range(0, 2);

        if (site == 0)
            return  -6.0f;
        else
            return 15.0f;
    }

    public Vector3 DrawFishScale()
    {
        float size = Random.Range(minScale, maxScale);
        return new Vector3(size,size, size);
    }
}
