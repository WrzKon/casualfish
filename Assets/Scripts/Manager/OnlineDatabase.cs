﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System;

public class OnlineDatabase : MonoBehaviour
{
    public List<Score> scores = new List<Score>();

    #region Get
    public void GetBestScores()
    {
        StartCoroutine(GetScore());
    }

    IEnumerator GetScore()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://pum2017wrzmys.cba.pl/select.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                scores = webResponseParse(www.downloadHandler.text);
            }
        }
    }

    List<Score> webResponseParse(string text)
    {
        List<Score> result = new List<Score>();
        Match match = Regex.Match(text, "<div id=\"danezmojejgry\">.*</div>");
        if (match.Success)
        {
            string div = match.Value;
            MatchCollection matchList = Regex.Matches(div, @"[a-zA-Z0-9]+ \d+");
            matchList
                .Cast<Match>()
                .ToList()
                .ForEach(x =>
                {
                    string[] scoreString = x.Value.Split(' ');
                    result.Add(new Score(scoreString[0], int.Parse(scoreString[1])));
                }
            );
        }
        return result;
    }
    #endregion
    #region Post
    public void InsertAndGetBestScores(string nick, int points)
    {
        StartCoroutine(InsertScores(nick, points));
    }

    IEnumerator InsertScores(string nick, int points)
    {
        WWWForm form = new WWWForm();
        form.AddField("nick", nick);
        form.AddField("points", points);

        using (UnityWebRequest www = UnityWebRequest.Post("http://pum2017wrzmys.cba.pl/insert.php", form))
        {
            yield return www.Send();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }

        using (UnityWebRequest www = UnityWebRequest.Get("http://pum2017wrzmys.cba.pl/select.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                scores = webResponseParse(www.downloadHandler.text);
            }
        }
    }
    #endregion
}

[System.Serializable]
public class Score
{
    public string Nick { get; private set; }
    public int Points { get; private set; }

    public Score(string nick, int points)
    {
        this.Nick = nick;
        this.Points = points;
    }
}